export const createStatusOKResponse = (res, msg, data) => {
	res.status(200).json({ msg, data });
};

export const createCreatedResponse = (res, msg, data) => {
	res.status(201).json({ msg, data });
};

export const createBadRequestResponse = (res, msg, data) => {
	res.status(400).json({ msg, data });
};

export const createUnauthorizedResponse = (res, msg, data) => {
	res.status(401).json({ msg, data });
};

export const createForbiddenResponse = (res, msg, data) => {
	res.status(403).json({ msg, data });
};

export const createNotFoundResponse = (res, msg, data) => {
	res.status(404).json({ msg, data });
};

export const createConflictResponse = (res, msg, data) => {
	res.status(409).json({ msg, data });
};

export const createInternalServerErrorResponse = (res, msg, data) => {
	res.status(500).json({ msg, data });
};
export const checkSeriesReqBody = (obj) => {
	return (!obj.title || 
		!obj.created_by || 
		!obj.release_year || 
		!obj.seasons || 
		!obj.genre_id ||
		obj.title.length > 254 || 
		obj.created_by.length > 254 || 
		(isNaN(obj.release_year)) || 
		(isNaN(obj.seasons)) || 
		(isNaN(obj.genre_id)))
		? false : true;
};

export const checkMovieReqBody = (obj) => {
	return (!obj.title || 
		!obj.director ||
		!obj.release_year ||
		!obj.genre_id ||
		obj.title.length > 254 || 
		obj.director.length > 254 || 
		(isNaN(obj.release_year)) || 
		(isNaN(obj.genre_id))) 
		? false : true;
};

export const checkMusicReqBody = (obj) => {
	return (!obj.name ||
		!obj.artist ||
		!obj.label ||
		!obj.type ||
		!obj.release_year ||
		!obj.genre_id ||
		obj.name.length > 254 || 
		obj.artist.length > 254 || 
		obj.label.length > 254 ||
		obj.type.length > 254 ||
		(isNaN(obj.release_year)) || 
		(isNaN(obj.genre_id)))
		? false : true;
};

export const checkBookReqBody = (obj) => {
	return (!obj.name ||
		!obj.author ||
		!obj.publishing_year ||
		!obj.category_id ||
		obj.name.length > 254 || 
		obj.author.length > 254 || 
		(isNaN(obj.publishing_year)) || 
		(isNaN(obj.category_id)))
		? false : true;
};

export const isValidYear = (year) => {
	return (isNaN(year) || !year || isNaN(parseInt(year)) || parseInt(year) > new Date().getFullYear())
		? false
		: true;
};

export const checkPassword = (password) => {
	return password.length < 8 || password.length > 254
		? false
		: true;
};

export const checkName = (name) => {
	return name.length < 5 || name.length > 100
		? false 
		: true;
};

export const checkEmailLength = (email) => {
	return email.length < 10 || email.length > 254
		? false 
		: true;
};
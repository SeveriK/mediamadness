import jwt from "jsonwebtoken";
import process from "process";
import { findUserById } from "../queries/userQueries.js";

export const idFromToken = (req) => {
	const authHeader = req.headers["authorization"];
	const token = authHeader && authHeader.split(" ")[1];

	const decoded = jwt.verify(token, process.env.SECRET);
	const userId = parseInt(decoded.id);
	return userId;
};

export const isAdmin = (req, res, next) => {
	const authHeader = req.headers["authorization"];
	const token = authHeader && authHeader.split(" ")[1];

	const decoded = jwt.verify(token, process.env.SECRET);
	const admin = decoded.admin;
	admin !== true
		? res.status(400).send("Not allowed")
		: next();
};

export const authenticateToken = (req, res, next) => {
	const authHeader = req.headers["authorization"];
	const token = authHeader && authHeader.split(" ")[1];
	if(token == null) return res.status(401).send("Authentication failed");

	jwt.verify(token, process.env.SECRET, (err, user) => {
		if(err) return res.status(403).send("Not allowed");
		req.user = user;
		next();
	});
};

export const createToken = (id, admin) => {
	const accessToken = jwt.sign(
		{
			"id": id,
			"admin": admin
		}, 
		process.env.SECRET, {expiresIn: "50m"});
	return accessToken;
};

export const verifyRefreshToken = (req, res, id) => {
	const token = req.body.token;
	const user = findUserById(id);
	jwt.verify(token, process.env.REFRESH_TOKEN_SECRET, (err) => {
		if (err) return res.status(403).send("Can't verify token");
		const accessToken = createToken(id, user.admin);
		res.json({ accessToken: accessToken });
	});
};

export const createRefreshToken = (id, admin) => {
	const refreshToken = jwt.sign(
		{
			"id": id,
			"admin": admin
		},
		process.env.REFRESH_TOKEN_SECRET);
	return refreshToken;
};
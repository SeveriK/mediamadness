import pool from "../dbConfig.js";
import { idFromToken } from "../utilities/tokenUtilities.js";
import { findUserById } from "./userQueries.js";

export const getMusic = (req, res) => {
	pool.query(`
		SELECT name, artist, genre_name, release_year, label, type
		FROM music
		JOIN musicgenres
			ON musicgenres.genre_id = music.genre_id`,
	(err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getMusicByName = (req, res) => {
	const name = req.body.name;
	pool.query(`
		SELECT name, artist, genre_name, release_year, label, type
		FROM music
		JOIN musicgenres
			ON musicgenres.genre_id = music.genre_id
			WHERE name = $1`,
	[name], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getMusicByGenre = (req, res) => {
	const genre = req.body.genre;
	pool.query(`
		SELECT name, artist, genre_name, release_year, label, type
		FROM music
		JOIN musicgenres
			ON musicgenres.genre_id = music.genre_id
			WHERE genre_name = $1`,
	[genre], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getMusicByYear = (req, res) => {
	const year = req.body.year;
	pool.query(`
		SELECT name, artist, genre_name, release_year, label, type
		FROM music
		JOIN musicgenres
			ON musicgenres.genre_id = music.genre_id
		WHERE release_year = $1`,
	[year], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getMusicByArtist = (req, res) => {
	const artist = req.body.artist;
	pool.query(`
		SELECT name, artist, genre_name, release_year, label, type
		FROM music
		JOIN musicgenres
			ON musicgenres.genre_id = music.genre_id
			WHERE artist = $1`,
	[artist], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getMusicByUser = (req, res) => {
	const user = req.body.username;
	pool.query(`
		SELECT name, artist, genre_name, release_year, label, type, comments
		FROM music_collections
		JOIN users
			ON users.user_id = music_collections.user_id
		JOIN music
			ON music.music_id = music_collections.music_id
		JOIN musicgenres
			on musicgenres.genre_id = music.genre_id
			WHERE username = $1`,
	[user], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getMusicByType = (req, res) => {
	const type = req.body.type;
	pool.query(`
		SELECT name, artist, genre_name, release_year, label, type
		FROM music
		JOIN musicgenres
			ON musicgenres.genre_id = music.genre_id
			WHERE type = $1`,
	[type], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

const updateCollection = (req, res) => {
	const comments = req.body.comments;
	const music_id = req.params.music_id;
	pool.query(`
		UPDATE music_collections
		SET comments = $1
		WHERE music_id = $2`, 
	[comments, music_id], (err) => {
		(err) 
			? res.status(500).send("Something went wrong")
			: res.status(200).send("Updated successfully");
	});
};

const editMusic = (req, res) => {
	const music_id = req.params.music_id;
	const { name, artist, genre_id, release_year, label, type } = req.body;
	pool.query(`
		UPDATE music 
		SET name = $1,
			artist = $2,
			genre_id = $3,
			release_year = $4,
			label = $5,
			type = $6
		WHERE music_id = $7`,
	[name, artist, genre_id, release_year, label, type, music_id], (err) => {
		err 
			? res.status(500).send("Something went wrong")
			: updateCollection(req,res);
	});
};

const checkAuth = async (req, res) => {
	const user_id = idFromToken(req);
	const music_id = req.params.music_id;
	pool.query(`
		SELECT * FROM music_collections
		WHERE user_id = $1 
		AND music_id = $2`, 
	[user_id, music_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		(results.rows.length > 0)
			? editMusic(req, res)
			: res.status(401).send("Unauthorized");
	});
};

export const checkMusic = async (req, res) => {
	const music_id = req.params.music_id;
	const user_id = idFromToken(req);
	const user = await findUserById(user_id);
	pool.query(`
		SELECT * FROM music_collections
		WHERE music_id = $1`,
	[music_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		if(results.rows.length === 0) res.status(404).send("Not found");
		else {
			user.admin === true 
				? editMusic(req, res)
				: checkAuth(req, res);
		}
	});
};

const addToCollection = (user_id, music_id, comments, req, res) => {
	pool.query(`
		INSERT INTO music_collections (user_id, music_id, comments)
		VALUES ($1, $2, $3)`, 
	[user_id, music_id, comments], (err) => {
		err
			? res.status(409).send("Conflict")
			: res.status(200).send("Music added");
	});
};

const addMusic = (name, artist, genre_id, release_year, label, type, req, res) => {
	pool.query(`
		INSERT INTO music
			(name, artist, genre_id, release_year, label, type)
		VALUES ($1, $2, $3, $4, $5, $6)`,
	[name, artist, genre_id, release_year, label, type], (err) => {
		err
			? res.status(500).send("Something went wrong")
			: addNewMusic(req, res);
	});
};

export const addNewMusic = async (req, res) => {
	const id = idFromToken(req);
	const { name, artist, genre_id, release_year, label, type, comments } = req.body;
	
	pool.query(`
		SELECT * FROM music
		WHERE name = $1`, 
	[name], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		(results.rows.length === 0)
			? addMusic(name, artist, genre_id, release_year, label, type, req, res)
			: addToCollection(id, results.rows[0].music_id, comments, req, res);
	});
};

export const addMusicGenre = (req, res) => {

	const genre = req.body.genre;
	pool.query(`
		INSERT INTO musicgenres (genre_name) 
		VALUES ($1)`, 
	[genre], (err) => {
		err
			? res.status(500).send("Something went wrong")
			: res.status(200).send(`${genre} added to musicgenres`);
	});
};

const deleteMusic = (req, res) => {
	const music_id = req.params.music_id;
	pool.query(`
		DELETE FROM music_collections
		WHERE music_id = $1`,
	[music_id], (err) => {
		err
			? res.status(500).send("Something went wrong")
			: res.status(200).send("Music deleted");
	});
};

export const checkAuthForDelete = async (req, res) => {
	const user_id = idFromToken(req);
	const music_id = req.params.music_id;
	pool.query(`
		SELECT * FROM music_collections
		WHERE user_id = $1 
		AND music_id = $2`,
	[user_id, music_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		(results.rows.length > 0)
			? deleteMusic(req, res)
			: res.status(401).send("Not found");
	});
};

export const checkMusicToDelete = async (req, res) => {
	const music_id = req.params.music_id;
	const user_id = idFromToken(req);
	const user = await findUserById(user_id);
	pool.query(`
		SELECT * FROM music_collections
		WHERE music_id = $1`,
	[music_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		if(results.rows.length === 0) res.status(404).send("Not found");
		else {
			user.admin === true 
				? deleteMusic(req, res)
				: checkAuthForDelete(req, res);
		}
	});
};
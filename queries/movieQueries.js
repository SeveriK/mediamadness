import pool from "../dbConfig.js";
import { idFromToken } from "../utilities/tokenUtilities.js";
import { findUserById } from "./userQueries.js";

export const getMovies = (req, res) => {
	pool.query(`
		SELECT
			title, 
			director, 
			release_year,
			genre_name
		FROM movies
		INNER JOIN moviegenres
			ON moviegenres.genre_id = movies.genre_id `, 
	(err, results) => {
		err
			? res.status(500).send("Something went wrong")
			: res.status(200).json(results.rows);
	});
};

export const getMovieByTitle = (req, res) => {
	const title = req.body.title;
	pool.query(`
		SELECT 
			title, 
			director, 
			release_year,
			genre_name 
		FROM movies
		INNER JOIN moviegenres
			ON moviegenres.genre_id = movies.genre_id
		WHERE title = $1`, 
	[title], (err, results) => {
		if(results.rows.length === 0) {
			res.status(400).send("No movies found with that title");
		} else {
			err
				? res.status(500).send("Something went wrong")
				: res.status(200).json(results.rows);
		}
	});
};

export const getMoviesByGenre = (req, res) => {
	const genre = req.body.genre;
	pool.query(`
		SELECT
			title, 
			director, 
			release_year,
			genre_name			
		FROM movies
		INNER JOIN moviegenres
			ON moviegenres.genre_id = movies.genre_id 
		WHERE genre_name = $1`, 
	[genre], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(400).send(`No movies in genre: ${genre}`)
			: res.status(200).json(results.rows);
	});
};

export const getMoviesByReleaseYear = (req, res) => {
	const year = req.body.year;
	pool.query(`
		SELECT
			title, 
			director, 
			release_year,
			genre_name
		FROM movies
		JOIN moviegenres
			ON moviegenres.genre_id = movies.genre_id
		WHERE release_year = $1`, 
	[year], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getMoviesByDirector = (req, res) => {
	const director = req.body.director;
	pool.query(`
		SELECT
			title, 
			director, 
			release_year,
			genre_name
		FROM movies
		JOIN moviegenres
			ON moviegenres.genre_id = movies.genre_id
		WHERE director = $1`, 
	[director], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getMoviesByUsername = (req, res) => {
	const username = req.body.username;
	pool.query(`
		SELECT 
			title, 
			director, 
			release_year,
			genre_name,
			comments
			FROM movie_collections
		JOIN users
			ON users.user_id = movie_collections.user_id
		JOIN movies
			ON movies.movie_id = movie_collections.movie_id
		JOIN moviegenres
			ON moviegenres.genre_id = movies.genre_id
		WHERE username = $1`,
	[username], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

const updateCollection = (req, res) => {
	const comments = req.body.comments;
	const movie_id = req.params.movie_id;
	pool.query(`
		UPDATE movie_collections
		SET comments = $1
		WHERE movie_id = $2`, 
	[comments, movie_id], (err) => {
		(err) 
			? res.status(500).send("Something went wrong")
			: res.status(200).send("Updated successfully");
	});
};

const editMovie = (req, res) => {
	const movie_id = req.params.movie_id;
	const { title, director, release_year, genre_id } = req.body;
	pool.query(`
		UPDATE movies
		SET title = $1,
			director = $2,
			release_year = $3,
			genre_id = $4
		WHERE movie_id = $5`, 
	[title, director, release_year, genre_id, movie_id], (err) => {
		err
			? res.status(400).send("Can't update movie")
			: updateCollection(req, res);
	});
};

const checkAuth = async (req, res) => {
	const user_id = idFromToken(req);
	const movie_id = req.params.movie_id;
	pool.query(`
		SELECT * FROM movie_collections
		WHERE user_id = $1 
		AND movie_id = $2`, [user_id, movie_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		(results.rows.length > 0)
			? editMovie(req, res)
			: res.status(401).send("Unauthorized");
	});
};

export const checkMovie = async (req, res) => {
	const movie_id = req.params.movie_id;
	const user_id = idFromToken(req);
	const user = await findUserById(user_id);
	pool.query(`
		SELECT * FROM movie_collections
		WHERE movie_id = $1`,
	[movie_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		if(results.rows.length === 0) res.status(404).send("Not found");
		else {
			user.admin === true 
				? editMovie(req, res)
				: checkAuth(req, res);
		}
	});
};

const addToCollection = (user_id, movie_id, comments, req, res) => {
	pool.query(`
		INSERT INTO movie_collections (user_id, movie_id, comments)
		VALUES ($1, $2, $3)`, 
	[user_id, movie_id, comments], (err) => {
		err
			? res.status(409).send("Conflict")
			: res.status(200).send("Movie added");
	});
};

const addMovie = (title, director, release_year, genre_id, req, res) => {
	pool.query(`
		INSERT INTO movies (title, director, release_year, genre_id)
		VALUES ($1, $2, $3, $4)`,
	[title, director, release_year, genre_id], (err) => {
		err
			? res.status(500).send("Something went wrong")
			: newMovie(req, res);
	});
};

export const newMovie = async (req, res) => {
	const id = idFromToken(req);
	const { title, director, release_year, genre_id, comments } = req.body;
	
	pool.query(`
		SELECT * FROM movies
		WHERE title = $1`, 
	[title], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		(results.rows.length === 0) 
			? addMovie(title, director, release_year, genre_id)
			: addToCollection(id, results.rows[0].movie_id, comments, req, res);
	});
};

export const addMovieGenre = (req, res) => {
	const genre = req.body.genre;
	pool.query(`
		INSERT INTO moviegenres (genre_name) 
		VALUES ($1)`, 
	[genre], (err) => {
		err
			? res.status(500).send("Something went wrong")
			: res.status(200).send(`${genre} added to moviegenres`);
	});
};

const deleteMovie = (req, res) => {
	const movie_id = req.params.movie_id;
	pool.query(`
		DELETE FROM movie_collections
		WHERE movie_id = $1`,
	[movie_id], (err) => {
		err
			? res.status(500).send("Something went wrong")
			: res.status(200).send("Movie deleted");
	});
};

const checkAuthForDelete = async (req, res) => {
	const user_id = idFromToken(req);
	const movie_id = req.params.movie_id;
	pool.query(`
		SELECT * FROM movie_collections
		WHERE user_id = $1 
		AND movie_id = $2`,
	[user_id, movie_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		(results.rows.length > 0)
			? deleteMovie(req, res)
			: res.status(401).send("Unauthorized");
	});
};

export const checkMovieToDelete = async (req, res) => {
	const movie_id = req.params.movie_id;
	const user_id = idFromToken(req);
	const user = await findUserById(user_id);
	pool.query(`
		SELECT * FROM movie_collections
		WHERE movie_id = $1`,
	[movie_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		if(results.rows.length === 0) res.status(404).send("Not found");
		else {
			user.admin === true 
				? deleteMovie(req, res)
				: checkAuthForDelete(req, res);
		}
	});
};
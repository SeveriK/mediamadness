import pool from "../dbConfig.js";
import bcrypt from "bcrypt";

import {
	createRefreshToken,
	createToken
} from "../utilities/tokenUtilities.js";

import { 
	checkEmailLength, 
	checkName 
} from "../utilities/validation.js";

export const getUser = (req, res) => {
	const username = req.params.username;
	pool.query(`
		SELECT * FROM users
		WHERE username = $1`, 
	[username], (err, results) => {
		err
			? res.status(404).send("Not found")
			: res.status(200).json(results.rows);
	});
};

export const getUsers = (req, res) => {
	pool.query(`
		SELECT * FROM users`, 
	(err, results) => {
		err
			? res.status(500).send("Something went wrong")
			: res.status(200).json(results.rows);
	});
};

export const getUserById = (req, res) => {
	const id = req.params.id;
	pool.query(`
		SELECT * FROM users
		WHERE user_id = $1`, 
	[id], (err, results) => {
		if(err)res.status(500).send("Something went wrong");
		!results.rows.length
			? res.status(404).send("Not found")
			: res.status(200).json(results.rows);
	});
};

export const findUserById = (id) => {

	return new Promise(function(resolve, reject){
		pool.query(
			"SELECT * FROM users WHERE user_id = $1",
			[id], (err, results) => {
				if(err) 
					return reject(err);
				resolve(results.rows[0]);
			});
	});
};

export const findUser = (email) => {

	return new Promise(function(resolve, reject){
		pool.query(
			"SELECT * FROM users WHERE email = $1",
			[email], (err, results) => {
				if(err) 
					return reject(err);
				resolve(results.rows[0]);
			});
	});
};

const addUserToDb = async (req, res) => {
	const username = req.body.username;
	const email = req.body.email;
	const salt = await bcrypt.genSalt();
	const password = await bcrypt.hash(req.body.password, salt);
	const admin = false;
	pool.query(`
		INSERT INTO users (username, email, password, joined_at, admin)
		VALUES ($1, $2, $3, LOCALTIMESTAMP, $4)`, 
	[username, email, password, admin], (err) => {
		err
			? res.status(500).send("Registration failed")
			: res.status(200).send("Registered succesfully");
	});
};

const findEmail = (req, res) => {
	const email = req.body.email;
	if(!checkEmailLength(email)) res.status(400).send("Email must be at least 10 characters");
	else {
		pool.query(`
			SELECT * FROM users
			WHERE email = $1`, 
		[email], (err, results) => {
			if(err) console.log(err);
			results.rows.length > 0
				? res.status(401).send("Email already in use")
				: addUserToDb(req, res);
		});
	}
};

export const checkUsername = (req, res) => {
	const username = req.body.username;
	if(!checkName(username)) res.status(400).send("Username must be between 5 and 100 characters");
	else {
		pool.query(`
			SELECT * FROM users
			WHERE username = $1`, 
		[username], (err, results) => {
			if(err) console.log(err);
			results.rows.length > 0
				? res.status(409).send("Username taken")
				: findEmail(req, res);
		});
	}
};

export const verifyPassword = async (req, res) => {
	const { email, password } = req.body;
	const user = await findUser(email);
	try {
		if(await bcrypt.compare(password, user.password)){
			const accessToken = createToken(user.user_id, user.admin);
			const refreshToken = createRefreshToken(user.user_id, user.admin);
			res.json({accesstoken: accessToken, refreshtoken: refreshToken});
		} else {
			res.status(400).send("Wrong password");
		}
	}
	catch {
		res.status(500).send("Something went wrong");
	}
};

export const loginUser = async (req, res) => {
	!await findUser(req.body.email)
		? res.status(404).send("Not found")
		: verifyPassword(req, res);
};

export const deleteUserFromDb = (req, res) => {
	const id = req.params.id;
	pool.query(`
		DELETE FROM users
		WHERE user_id= $1`, 
	[id], (err) => {
		err
			? res.status(500).send("Can't delete user")
			: res.status(200).send("User deleted");
	});
};
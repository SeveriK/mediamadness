import pool from "../dbConfig.js";

export const getBooks = async () => {
	const result = await pool.query(
		`SELECT name, author, publishing_year, category_name
		FROM books
		JOIN bookcategories
			ON bookcategories.category_id = books.category_id`,
	);
	return result.rows;
};

export const getBookByID = async (Id) => {
	const result = await pool.query(
		`SELECT * FROM books
		WHERE book_id = $1`,
		[Id]
	);
	return result.rows[0];
};

export const bookDoesExist = async (Id) => {
	const result = await pool.query(
		`SELECT * FROM books
		WHERE book_id = $1`,
		[Id]
	);
	return result.rows[0] ? true : false;
};

export const isBookInCollection = async (book_id, user_id) => {
	const result = await pool.query(
		`SELECT * FROM book_collections
		WHERE book_id = $1
		AND user_id = $2`,
		[book_id, user_id]
	);
	return result.rows[0] ? true : false;
};

export const getBookByName = async (name) => {
	const result = await pool.query(
		`SELECT book_id, name, author, publishing_year, category_name
		FROM books
		JOIN bookcategories
			ON bookcategories.category_id = books.category_id
			WHERE name = $1`,
		[name],
	);
	return result.rows[0];
};

export const getBooksByAuthor = async (author) => {
	const result = await pool.query(
		`SELECT name, author, publishing_year, category_name
		FROM books
		JOIN bookcategories
			ON bookcategories.category_id = books.category_id
			WHERE author = $1`,
		[author],
	);
	return result.rows;
};

export const getBooksByYear = async (year) => {
	const result = await pool.query(
		`SELECT name, author, publishing_year, category_name
		FROM books
		JOIN bookcategories
			ON bookcategories.category_id = books.category_id
		WHERE publishing_year = $1`,
		[year],
	);
	return result.rows;
};

export const getBooksByCategory = async (category) => {
	const result = await pool.query(
		`SELECT name, author, publishing_year, category_name
		FROM books
		JOIN bookcategories
			ON bookcategories.category_id = books.category_id
			WHERE category_name = $1`,
		[category], 
	);
	return result.rows;
};

export const getBooksByUser = async (user) => {
	const result = await pool.query(
		`SELECT name, author, publishing_year, category_name, comments
		FROM book_collections
		JOIN users
			ON users.user_id = book_collections.user_id
		JOIN books
			ON books.book_id = book_collections.book_id
		JOIN bookcategories
			on bookcategories.category_id = books.category_id
			WHERE username = $1`,
		[user], 
	);
	return result.rows;
};

export const updateCollection = async (comment, book_id) => {
	const result = await pool.query(
		`UPDATE book_collections
		SET comments = $1
		WHERE book_id = $2
		RETURNING *`, 
		[comment, book_id], 
	);
	return result.rows[0] ? true : false;
};

export const editBook = async (name, author, publishing_year, category_id, book_id) => {
	const result = await pool.query(`
		UPDATE books
		SET name = $1,
			author = $2,
			publishing_year = $3,
			category_id = $4
		WHERE book_id = $5
		RETURNING *`,
	[name, author, publishing_year, category_id, book_id], 
	);
	return result.rows[0];
};

export const isBookAccessible = async (user_id, book_id) => {
	const result = await pool.query(`
		SELECT * FROM book_collections
		WHERE user_id = $1 
		AND book_id = $2`, 
	[user_id, book_id],
	);
	return result.rows[0] ? true : false;
};

export const addToCollection = async (user_id, book_id, comments) => {
	const result = await pool.query(
		`INSERT INTO book_collections (user_id, book_id, comments)
		VALUES ($1, $2, $3)
		RETURNING *`, 
		[user_id, book_id, comments],
	);
	return result.rows[0];
};

export const addNewBook = async (name, author, publishing_year, category_id) => {
	const result = await pool.query(
		`INSERT INTO books
			(name, author, publishing_year, category_id)
		VALUES ($1, $2, $3, $4)
		RETURNING *`,
		[name, author, publishing_year, category_id],
	);
	return result.rows[0];
};

export const categoryDoesExist = async (category) => {
	const result = await pool.query(
		`SELECT * FROM bookcategories
		WHERE category_name = $1`,
		[category],
	);
	return result.rows[0] ? true : false;
};

export const addBookCategory = async (category) => {
	const result = await pool.query(
		`INSERT INTO bookcategories (category_name) 
		VALUES ($1)
		RETURNING *`, 
		[category],
	);
	return result.rows[0];
};

export const deleteBook = async (book_id) => {
	const result = await pool.query(
		`DELETE FROM books
		WHERE book_id = $1
		RETURNING *`,
		[book_id],
	);
	return result.rows[0];
};

export const deleteBookFromCollections = async (book_id) => {
	const result = await pool.query(
		`DELETE FROM book_collections
		WHERE book_id = $1
		RETURNING *`,
		[book_id],
	);
	return result.rows[0];
};

import pool from "../dbConfig.js";
import { idFromToken } from "../utilities/tokenUtilities.js";
import { findUserById } from "./userQueries.js";

export const getSeries = (req, res) => {
	pool.query(`
		SELECT
			title, 
			created_by, 
			release_year,
			genre_name
		FROM series
		INNER JOIN moviegenres
			ON moviegenres.genre_id = series.genre_id `, 
	(err, results) => {
		err
			? res.status(500).send("Something went wrong")
			: res.status(200).json(results.rows);
	});
};

export const getSeriesByTitle = (req, res) => {
	const title = req.body.title;
	pool.query(`
		SELECT 
			title, 
			created_by, 
			release_year,
			seasons,
			genre_name 
		FROM series
		INNER JOIN moviegenres
			ON moviegenres.genre_id = series.genre_id
		WHERE title = $1`, 
	[title], (err, results) => {
		if(results.rows.length === 0) {
			res.status(204).send("No content");
		} else {
			err
				? res.status(500).send("Something went wrong")
				: res.status(200).json(results.rows);
		}
	});
};

export const getSeriesByGenre = (req, res) => {
	const genre = req.body.genre;
	pool.query(`
		SELECT
			title, 
			created_by, 
			release_year,
			seasons,
			genre_name			
		FROM series
		INNER JOIN moviegenres
			ON moviegenres.genre_id = series.genre_id 
		WHERE genre_name = $1`, 
	[genre], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getSeriesByReleaseYear = (req, res) => {
	const year = req.body.year;
	pool.query(`
		SELECT
			title, 
			created_by, 
			release_year,
			seasons,
			genre_name
		FROM series
		JOIN moviegenres
			ON moviegenres.genre_id = series.genre_id
		WHERE release_year = $1`, 
	[year], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getSeriesByCreator = (req, res) => {
	const created_by = req.body.created_by;
	pool.query(`
		SELECT
			title, 
			created_by, 
			release_year,
			seasons,
			genre_name
		FROM series
		JOIN moviegenres
			ON moviegenres.genre_id = series.genre_id
		WHERE created_by = $1`, 
	[created_by], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

export const getSeriesByUsername = (req, res) => {
	const username = req.body.username;
	pool.query(`
		SELECT 
			title, 
			created_by, 
			release_year,
			seasons,
			genre_name,
			comments
			FROM series_collections
		JOIN users
			ON users.user_id = series_collections.user_id
		JOIN series
			ON series.series_id = series_collections.series_id
		JOIN moviegenres
			ON moviegenres.genre_id = series.genre_id
		WHERE username = $1`,
	[username], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		results.rows.length === 0
			? res.status(204).send("No content")
			: res.status(200).json(results.rows);
	});
};

const updateCollection = (req, res) => {
	const comments = req.body.comments;
	const series_id = req.params.series_id;
	pool.query(`
		UPDATE series_collections
		SET comments = $1
		WHERE series_id = $2`, 
	[comments, series_id], (err) => {
		(err) 
			? res.status(500).send("Something went wrong")
			: res.status(200).send("Updated successfully");
	});
};

const editSeries = (req, res) => {
	const series_id = req.params.series_id;
	const { title, created_by, release_year, seasons, genre_id } = req.body;
	pool.query(`
		UPDATE series
		SET title = $1,
			created_by = $2,
			release_year = $3,
			seasons = $4,
			genre_id = $5
		WHERE series_id = $6`, 
	[title, created_by, release_year, seasons, genre_id, series_id], (err) => {
		err
			? res.status(400).send("Can't update series")
			: updateCollection(req, res);
	});
};

const checkAuth = async (req, res) => {
	const user_id = idFromToken(req);
	const series_id = req.params.series_id;
	pool.query(`
		SELECT * FROM series_collections
		WHERE user_id = $1
		AND series_id = $2`,
	[user_id, series_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		(results.rows.length > 0)
			? editSeries(req, res)
			: res.status(401).send("Unauthorized");
	});
};

export const checkSeries = async (req, res) => {
	const series_id = req.params.series_id;
	const user_id = idFromToken(req);
	const user = await findUserById(user_id);
	pool.query(`
		SELECT * FROM series_collections
		WHERE series_id = $1`,
	[series_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		if(results.rows.length === 0) res.status(404).send("Not found");
		else {
			user.admin === true 
				? editSeries(req, res)
				: checkAuth(req, res);
		}
	});
};

const addToCollection = (user_id, series_id, comments, req, res) => {
	pool.query(`
		INSERT INTO series_collections (user_id, series_id, comments)
		VALUES ($1, $2, $3)`,
	[user_id, series_id, comments], (err) => {
		err
			? res.status(409).send("Conflict")
			: res.status(200).send("Series added");
	});
};

const addSeries = (title, created_by, release_year, seasons, genre_id, req, res) => {
	pool.query(`
		INSERT INTO series (title, created_by, release_year, seasons, genre_id)
		VALUES ($1, $2, $3, $4, $5)`,
	[title, created_by, release_year, seasons, genre_id], (err) => {
		err
			? res.status(500).send("Something went wrong")
			: newSeries(req, res);
	});
};

export const newSeries = (req, res) => {
	const id = idFromToken(req);
	const { title, created_by, release_year, seasons, genre_id, comments } = req.body;
	
	pool.query(`
		SELECT * FROM series
		WHERE title = $1`, 
	[title], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		if(results.rows.length === 0) {
			addSeries(title, created_by, release_year, seasons, genre_id);
		} else {
			addToCollection(id, results.rows[0].series_id, comments, req, res);
		}
	});
};

const deleteSeries = (id, req, res) => {
	const series_id = req.params.series_id;
	pool.query(`
		DELETE FROM series_collections
		WHERE series_id = $1`,
	[series_id, id], (err) => {
		err
			? res.status(500).send("Something went wrong")
			: res.status(200).send("Series deleted");
	});
};

export const checkAuthForDelete = async (req, res) => {
	const user_id = idFromToken(req);
	const series_id = req.params.series_id;
	pool.query(`
		SELECT * FROM series_collections
		WHERE user_id = $1 
		AND series_id = $2`,
	[user_id, series_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		(results.rows.length > 0)
			? deleteSeries(user_id, req, res)
			: res.status(401).send("Unauthorized");
	});
};

export const checkSeriesToDelete = async (req, res) => {
	const series_id = req.params.series_id;
	const user_id = idFromToken(req);
	const user = await findUserById(user_id);
	pool.query(`
		SELECT * FROM series_collections
		WHERE series_id = $1`,
	[series_id], (err, results) => {
		if(err) res.status(500).send("Something went wrong");
		if(results.rows.length === 0) res.status(404).send("Not found");
		else {
			user.admin === true 
				? deleteSeries(req, res)
				: checkAuthForDelete(req, res);
		}
	});
};

## Media Madness

## Description

Back-end for media listing system.
You can list medias you own and see other people's medias(movies, series, music, books). When you list a movie for example, it goes to the list with all movies listed by you and other users. It also goes to your personal collection, wich you can add to and delete items from. When adding items to collections, you can also add comments about them.


## Usage

Example:

path: http://localhost:7000/books

output: List of all books as json

## Endpoints

Books

| Method      | Path          | Description                          | 
| ----------- | ------------- | ------------------------------------ |
| GET     | /books        | Returns all books by all users       |
| GET     | /books/name   | Returns a book by given name         |
| GET     | /books/author | Returns books by given author       |
| GET     | /books/year   | Returns books by given year         |
| GET     | /books/category | Returns books by given category |
| GET     | /books/user   | Returns books listed by given user    |
| PUT     | /books/:id   | Edit a book that exists in personal collection |
| POST     | /books/new   | Adds new book to the list    |
| POST     | /books/newCategory   | Admin can add a new category   |
| DELETE   | /books/:book_id  | Delete a book from personal collection |



Movies

| Method      | Path          | Description                          | 
| ----------- | ------------- | ------------------------------------ |
| GET     | /movies        | Returns all movies by all users       |
| GET     | /movies/title   | Returns a movie by given title         |
| GET     | /movies/genre | Returns movies by given genre |
| GET     | /movies/year   | Returns movies by given year         |
| GET     | /movies/director | Returns movies by given director       |
| GET     | /movies/username   | Returns movies listed by given user    |
| PUT     | /movies/:id   | Edit a movie that exists in personal collection |
| POST     | /movies/new   | Adds new movie to the list    |
| POST     | /movies/newgenre   | Admin can add a new genre   |
| DELETE   | /movies/delete/:movie_id  | Delete a movie from personal collection |



Series

| Method      | Path          | Description                          | 
| ----------- | ------------- | ------------------------------------ |
| GET     | /series        | Returns all series by all users       |
| GET     | /series/title   | Returns a series by given title         |
| GET     | /series/genre | Returns series by given genre |
| GET     | /series/year   | Returns series by given year         |
| GET     | /series/creator | Returns series by given creator       |
| GET     | /series/username   | Returns series listed by given user    |
| PUT     | /series/:id   | Edit a series that exists in personal collection |
| POST     | /series/new   | Adds new series to the list    |
| POST     | /series/newgenre   | Admin can add a new genre   |
| DELETE   | /series/:series_id  | Delete a series from personal collection |

Music

| Method      | Path          | Description                          | 
| ----------- | ------------- | ------------------------------------ |
| GET     | /music        | Returns all music by all users       |
| GET     | /music/name   | Returns a music by given name         |
| GET     | /music/genre | Returns music by given genre |
| GET     | /music/year   | Returns music by given year         |
| GET     | /music/artist | Returns music by given artist       |
| GET     | /music/user| Returns music by given user       |
| GET     | /music/type  | Returns music listed by given type    |
| PUT     | /music/:id   | Edit a music that exists in personal collection |
| POST     | /music/new   | Adds new music to the list    |
| POST     | /music/newgenre   | Admin can add a new genre   |
| DELETE   | /music/:music_id  | Delete a music from personal collection |



## Contact

severi.kaakko@gmail.com

## Project status

Needs a lot of cleaning and refactoring
Needs front-end

Latest changes:
Cleaned and refactored book handling code.
Created a file for creating responses

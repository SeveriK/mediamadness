import express from "express";
import { 
	addNewMovie, 
	deleteMovieFromDb, 
	movieByGenre, 
	movieByReleaseYear, 
	movieByTitle, 
	moviesByDirector, 
	moviesByUsername, 
	newMovieGenre, 
	updateMovies
} from "../controllers/movieController.js";
import { getMovies } from "../queries/movieQueries.js";
import { 
	isAdmin,
	authenticateToken
} from "../utilities/tokenUtilities.js";

const movieRouter = express.Router();

movieRouter.get("/", (req, res) => {
	getMovies(req, res);
});

movieRouter.get("/title", (req, res) => {
	movieByTitle(req, res);
});

movieRouter.get("/genre", (req, res) => {
	movieByGenre(req, res);
});

movieRouter.get("/year", (req, res) => {
	movieByReleaseYear(req, res);
});

movieRouter.get("/director", (req, res) => {
	moviesByDirector(req, res);
});

movieRouter.get("/username", (req, res) => {
	moviesByUsername(req, res);
});

movieRouter.put("/:movie_id", authenticateToken, (req, res) => {
	updateMovies(req, res);
});

movieRouter.post("/new", authenticateToken, (req, res) => {
	addNewMovie(req, res);
});

movieRouter.post("/newGenre", isAdmin, (req, res) => {
	newMovieGenre(req, res);
});

movieRouter.delete("/delete/:movie_id", authenticateToken, (req, res) => {
	deleteMovieFromDb(req, res);
});

export default movieRouter;
import express from "express";
import {
	getBookByNameController,
	getBooksByAuthorController,	
	getBooksController,
	getBooksByYearController,
	getBooksByCategoryController,
	getBooksByUserController,
	updateBookController,
	addNewBookController,
	newBookCategoryController,
	deleteBookController
} from "../controllers/bookController.js";

import { 
	authenticateToken,
	isAdmin 
} from "../utilities/tokenUtilities.js";

const bookRouter = express.Router();

bookRouter.get("/", getBooksController);

bookRouter.get("/name", getBookByNameController);

bookRouter.get("/author", getBooksByAuthorController);

bookRouter.get("/year", getBooksByYearController);

bookRouter.get("/category", getBooksByCategoryController);

bookRouter.get("/user", getBooksByUserController);

bookRouter.put("/:id", authenticateToken, updateBookController);

bookRouter.post("/new", authenticateToken, addNewBookController);

bookRouter.post("/category", isAdmin, newBookCategoryController);

bookRouter.delete("/:id", authenticateToken, deleteBookController);

export default bookRouter;
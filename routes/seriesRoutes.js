import express from "express";
import { 
	addNewSeries, 
	deleteSeriesFromDb, 
	seriesByGenre, 
	seriesByReleaseYear, 
	seriesByTitle, 
	seriesByCreator, 
	seriesByUsername,  
	updateSeries
} from "../controllers/seriesController.js";
import { getSeries } from "../queries/seriesQueries.js";
import { authenticateToken } from "../utilities/tokenUtilities.js";

const seriesRouter = express.Router();

seriesRouter.get("/", (req, res) => {
	getSeries(req, res);
});

seriesRouter.get("/title", (req, res) => {
	seriesByTitle(req, res);
});

seriesRouter.get("/genre", (req, res) => {
	seriesByGenre(req, res);
});

seriesRouter.get("/year", (req, res) => {
	seriesByReleaseYear(req, res);
});

seriesRouter.get("/creator", (req, res) => {
	seriesByCreator(req, res);
});

seriesRouter.get("/username", (req, res) => {
	seriesByUsername(req, res);
});

seriesRouter.put("/:series_id", authenticateToken, (req, res) => {
	updateSeries(req, res);
});

seriesRouter.post("/new", authenticateToken, (req, res) => {
	addNewSeries(req, res);
});

seriesRouter.delete("/:series_id", authenticateToken, (req, res) => {
	deleteSeriesFromDb(req, res);
});

export default seriesRouter;
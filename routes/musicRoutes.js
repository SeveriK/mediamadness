import express from "express";
import {
	musicByName,
	musicByGenre,
	musicByYear,
	musicByArtist,
	musicByUser,
	musicByType,
	updateMusic,
	newMusic,
	newMusicGenre,
	deleteMusic
} from "../controllers/musicController.js";
import { getMusic } from "../queries/musicQueries.js";

import { authenticateToken, isAdmin } from "../utilities/tokenUtilities.js";

const musicRouter = express.Router();

musicRouter.get("/", (req, res) => {
	getMusic(req, res);
});

musicRouter.get("/name", (req, res) => {
	musicByName(req, res);
});

musicRouter.get("/genre", (req, res) => {
	musicByGenre(req, res);
});

musicRouter.get("/year", (req, res) => {
	musicByYear(req, res);
});

musicRouter.get("/artist", (req, res) => {
	musicByArtist(req, res);
});

musicRouter.get("/user", (req, res) => {
	musicByUser(req, res);
});

musicRouter.get("/type", (req, res) => {
	musicByType(req, res);
});

musicRouter.put("/:music_id", authenticateToken, (req, res) => {
	updateMusic(req, res);
});

musicRouter.post("/new", authenticateToken, (req, res) => {
	newMusic(req, res);
});

musicRouter.post("/newGenre", isAdmin, (req, res) => {
	newMusicGenre(req, res);
});

musicRouter.delete("/:music_id", authenticateToken, (req, res) => {
	deleteMusic(req, res);
});

export default musicRouter;
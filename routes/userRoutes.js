import express from "express";
import { 
	register,
	login,
	deleteUser,
	getUserByUsername,
	checkId
} from "../controllers/userController.js";

import {
	authenticateToken,
	verifyRefreshToken,
	idFromToken,
	isAdmin
} from "../utilities/tokenUtilities.js";

import {
	getUsers,
} from "../queries/userQueries.js";

const userRouter = express.Router();

userRouter.get("/admin", isAdmin, (req, res) => {
	res.send("Hello admin");
});

userRouter.get("/protected", authenticateToken, (req, res) => {
	res.send("Successfull authentication");
});

userRouter.get("/", (req, res) => {
	getUsers(req, res);
});

userRouter.get("/:id", (req, res) => {
	checkId(req, res);
});

userRouter.get("/name/:username", (req, res) => {
	getUserByUsername(req, res);
});

userRouter.post("/token", (req, res) => {
	const id = idFromToken(req);
	verifyRefreshToken(req, res, id);
});

userRouter.post("/register", (req, res) => {
	register(req, res);
});

userRouter.post("/login", (req, res) => {
	login(req, res);
});

userRouter.delete("/:id", (req, res) => {
	deleteUser(req, res);
});

export default userRouter;

import dotenv from "dotenv";
dotenv.config();
import express from "express";
import bodyParser from "body-parser";
import userRouter from "./routes/userRoutes.js";
import movieRouter from "./routes/movieRoutes.js";
import seriesRouter from "./routes/seriesRoutes.js";
import musicRouter from "./routes/musicRoutes.js";
import bookRouter from "./routes/bookRoutes.js";

const app = express();

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/user", userRouter);
app.use("/movies", movieRouter);
app.use("/series", seriesRouter);
app.use("/music", musicRouter);
app.use("/books", bookRouter);

const port = 7000;

app.listen(port, () => {
	console.log(`Server listening on port ${port}`);
});
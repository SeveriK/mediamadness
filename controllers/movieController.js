import { 
	addMovieGenre,
	getMoviesByGenre,
	getMovieByTitle,
	newMovie,
	getMoviesByReleaseYear,
	getMoviesByDirector,
	getMoviesByUsername,
	checkMovie,
	checkMovieToDelete
} from "../queries/movieQueries.js";
import { checkMovieReqBody } from "../utilities/validation.js";

export const movieByTitle = (req, res) => {
	!req.body.title
		? res.status(400).send("Enter a movie title")
		: getMovieByTitle(req, res);
};

export const movieByGenre = (req, res) => {
	!req.body.genre
		? res.status(400).send("No genre parameter found")
		: getMoviesByGenre(req, res);
};

export const movieByReleaseYear = (req, res) => {
	!req.body.year
		? res.status(400).send("Enter a year")
		: getMoviesByReleaseYear(req, res);
};

export const moviesByDirector = (req, res) => {
	!req.body.director
		? res.status(400).send("Enter director")
		: getMoviesByDirector(req, res);
};

export const moviesByUsername = (req, res) => {
	!req.params.username
		? res.status(400).send("Enter username")
		: getMoviesByUsername(req, res);
};

const yearCheck = (req, res) => {
	const year = parseInt(req.body.release_year);
	year > new Date().getFullYear() || year < 1887
		? res.status(400).send("Enter a valid year")
		: checkMovie(req, res);
};

export const updateMovies = (req, res) => { 
	checkMovieReqBody(req.body)
		? yearCheck(req, res)
		: res.status(400).send("Bad request");
};

export const addNewMovie = (req, res) => {
	checkMovieReqBody(req.body)
		? newMovie(req, res)
		: res.status(400).send("Bad request");
};

export const newMovieGenre = (req, res) => {
	!req.body.genre
		? res.status(400).send("Enter genre")
		: addMovieGenre(req, res);
};

export const deleteMovieFromDb = (req, res) => {
	!req.params.movie_id
		? res.status(400).send("MovieID parameter missing")
		: checkMovieToDelete(req, res);
};
import {
	addBookCategory,
	addNewBook,
	getBookByName,
	getBooksByAuthor,
	getBooksByYear,
	getBooksByCategory,
	getBooksByUser,
	getBooks,
	bookDoesExist,
	isBookAccessible,
	editBook,
	updateCollection,
	isBookInCollection,
	addToCollection,
	categoryDoesExist,
	deleteBook,
	deleteBookFromCollections
} from "../queries/bookQueries.js";
import { findUserById } from "../queries/userQueries.js";
import {
	createBadRequestResponse,
	createConflictResponse,
	createInternalServerErrorResponse,
	createNotFoundResponse, createStatusOKResponse,
	createUnauthorizedResponse
} from "../utilities/responses.js";
import { idFromToken } from "../utilities/tokenUtilities.js";
import {
	checkBookReqBody,
	isValidYear
} from "../utilities/validation.js";

export const getBooksController = async (req, res) => {
	const books = await getBooks();
	!books.length
		? createNotFoundResponse(res, "Not found")
		: createStatusOKResponse(res, "Books", books);
};

export const getBookByNameController = async (req, res) => {
	const name = req.body.name;
	if (!name) {
		createBadRequestResponse(res, "No name");
	} else {
		const book = await getBookByName(name);
		!book 
			? createNotFoundResponse(res, "No content")
			: createStatusOKResponse(res, "Book", book);
	}
};

export const getBooksByAuthorController = async (req, res) => {
	const author = req.body.author; 
	if (!author) {
		createBadRequestResponse(res, "No author");
	} else {
		const books = await getBooksByAuthor(author);
		!books.length
			? createNotFoundResponse(res, "Not found")
			: createStatusOKResponse(res, "Books", books);
	}
};

export const getBooksByYearController = async (req, res) => {
	const year = req.body.year;
	if (!year) {
		createBadRequestResponse(res, "No year");
	} else {
		const books = await getBooksByYear(year);
		!books.length
			? createNotFoundResponse(res, "Not found")
			: createStatusOKResponse(res, "Books", books);
	}
};

export const getBooksByCategoryController = async (req, res) => {
	const category = req.body.category;
	if (!category) {
		createBadRequestResponse(res, "No category");
	} else {
		const books = await getBooksByCategory(category);
		!books.length 
			? createNotFoundResponse(res, "Not found")
			: createStatusOKResponse(res, "Books", books);
	}
};

export const getBooksByUserController = async (req, res) => {
	const user = req.body.username;
	if (!user) {
		createBadRequestResponse(res, "No username");
	} else {
		const books = await getBooksByUser(user);
		!books.length 
			? createNotFoundResponse(res, "Not found")
			: createStatusOKResponse(res, "Books", books);
	}
};

export const updateBookController = async (req, res) => {
	const book_id = req.params.id;
	const user_id = idFromToken(req);
	const { name, author, publishing_year, category_id, comment } = req.body;
	if (checkBookReqBody(req.body) && isValidYear(req.body.publishing_year) && book_id) {
		if (await bookDoesExist(book_id)) {
			if (await isBookAccessible(user_id, book_id)) {
				const editedBook = await editBook(name, author, publishing_year, category_id, book_id);
				const updateCollectionItem = await updateCollection(comment, book_id);
				(editedBook && updateCollectionItem)
					? createStatusOKResponse(res, "Book edited", editedBook)
					: createInternalServerErrorResponse(res, "Something went wrong");
			} else {
				createUnauthorizedResponse(res, "Unauthorized");
			}
		} else {
			createNotFoundResponse(res, "Not found");
		}
	} else {
		createBadRequestResponse(res, "Bad request");
	}
};

export const addNewBookController = async (req, res) => {
	const user_id = idFromToken(req);
	const { name, author, publishing_year, category_id, comment } = req.body;
	if (checkBookReqBody(req.body)) {
		if (!await getBookByName(name)) { 
			const book = await addNewBook(name, author, publishing_year, category_id);
			if (!await isBookInCollection(book.book_id, user_id)) {
				await addToCollection(user_id, book.book_id, comment);
			}
			createStatusOKResponse(res, "Book added", book);
		} else {
			const book = await getBookByName(name);
			if (!await isBookInCollection(book.book_id, user_id)){
				const bookToCollection = await addToCollection(user_id, book.book_id, comment);
				bookToCollection
					? createStatusOKResponse(res, "Book added to collection")
					: createInternalServerErrorResponse(res, "Something went wrong");
			} else {
				createConflictResponse(res, "Book already exists");
			}
		}
	} else {
		createBadRequestResponse(res, "Bad request");
	}
};

export const newBookCategoryController = async (req, res) => {
	const category = req.body.category;
	if (!category) {
		createBadRequestResponse(res, "No category");
	} if (await categoryDoesExist(category)) {
		createConflictResponse(res, "Category already exists");
	} else {
		const addedCategory = await addBookCategory(category);
		addedCategory
			? createStatusOKResponse(res, "Category added", addedCategory)
			: createInternalServerErrorResponse(res, "Something went wrong");
	}
};

export const deleteBookController = async (req, res) => {
	const user_id = idFromToken(req);
	const book_id = req.params.id;
	const user = await findUserById(user_id);
	if (await bookDoesExist(book_id)) {
		if (await isBookAccessible(user_id, book_id) || user.admin) {
			if (await isBookInCollection(book_id)){
				await deleteBookFromCollections(book_id);
			}
			const deletedBook = await deleteBook(book_id);
			deletedBook
				? createStatusOKResponse(res, "Book deleted", deletedBook)
				: createInternalServerErrorResponse(res, "Something went wrong");
		} else {
			createUnauthorizedResponse(res, "Unauthorized");
		}
	} else {
		createNotFoundResponse(res, "Not found");
	}
};
import { 
	getSeriesByGenre,
	getSeriesByTitle,
	newSeries,
	getSeriesByReleaseYear,
	getSeriesByCreator,
	getSeriesByUsername,
	checkSeries,
	checkSeriesToDelete
} from "../queries/seriesQueries.js";
import {
	checkSeriesReqBody,
} from "../utilities/validation.js";

export const seriesByTitle = (req, res) => {
	!req.body.title
		? res.status(400).send("Enter a series title")
		: getSeriesByTitle(req, res);
};

export const seriesByGenre = (req, res) => {
	!req.body.genre
		? res.status(400).send("No genre parameter found")
		: getSeriesByGenre(req, res);
};

export const seriesByReleaseYear = (req, res) => {
	!req.body.year
		? res.status(400).send("Enter a year")
		: getSeriesByReleaseYear(req, res);
};

export const seriesByCreator = (req, res) => {
	!req.body.created_by
		? res.status(400).send("Enter creator")
		: getSeriesByCreator(req, res);
};

export const seriesByUsername = (req, res) => {
	!req.body.username
		? res.status(400).send("Enter username")
		: getSeriesByUsername(req, res);
};

const yearCheck = (req, res) => {
	const year = parseInt(req.body.release_year);
	year > new Date().getFullYear() || year < 1887
		? res.status(400).send("Enter a valid year")
		: checkSeries(req, res);
};

export const updateSeries = (req, res) => { 
	checkSeriesReqBody(req.body)
		? yearCheck(req, res)
		: res.status(400).send("Bad request");
};

export const addNewSeries = (req, res) => {
	checkSeriesReqBody(req.body)
		? newSeries(req, res)
		: res.status(400).send("Bad request");
};

export const deleteSeriesFromDb = (req, res) => {
	!req.params.series_id
		? res.status(400).send("SeriesID parameter missing")
		: checkSeriesToDelete(req, res);
};
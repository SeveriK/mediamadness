import {
	addMusicGenre,
	getMusicByName,
	getMusicByGenre,
	getMusicByYear,
	getMusicByArtist,
	getMusicByUser,
	getMusicByType,
	checkMusic,
	addNewMusic,
	checkMusicToDelete
} from "../queries/musicQueries.js";
import { checkMusicReqBody } from "../utilities/validation.js";

export const musicByName = (req, res) => {
	!req.body.name
		? res.status(400).send("Enter a name")
		: getMusicByName(req, res);
};

export const musicByGenre = (req, res) => {
	!req.body.genre
		? res.status(400).send("Enter a genre")
		: getMusicByGenre(req, res);
};

export const musicByYear = (req, res) => {
	!req.body.year
		? res.status(400).send("Enter a year")
		: getMusicByYear(req, res);
};

export const musicByArtist = (req, res) => {
	!req.body.artist
		? res.status(400).send("Enter an artist")
		: getMusicByArtist(req, res);
};

export const musicByUser = (req, res) => {
	!req.body.username
		? res.status(400).send("Enter a user")
		: getMusicByUser(req, res);
};

export const musicByType = (req, res) => {
	!req.body.type
		? res.status(400).send("Enter a type")
		: getMusicByType(req, res);
};

const yearCheck = (req, res) => {
	const year = parseInt(req.body.release_year);
	year > new Date().getFullYear() || year < 1887
		? res.status(400).send("Enter a valid year")
		: checkMusic(req, res);
};

export const updateMusic = (req, res) => {
	checkMusicReqBody(req.body)
		? yearCheck(req, res)
		: res.status(400).send("Bad request");
};

export const newMusic = (req, res) => {
	checkMusicReqBody(req.body)
		? addNewMusic(req, res)
		: res.status(400).send("Bad request");
};

export const newMusicGenre = (req, res) => {
	!req.body.genre
		? res.status(400).send("Enter a genre")
		: addMusicGenre(req, res);
};

export const deleteMusic = (req, res) => {
	!req.params.music_id
		? res.status(400).send("Enter an id")
		: checkMusicToDelete(req, res);
};
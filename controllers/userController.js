import { 
	checkUsername,
	findUserById,
	loginUser,
	deleteUserFromDb,
	getUser,
	getUserById
} from "../queries/userQueries.js";
import { checkPassword } from "../utilities/validation.js";
import { idFromToken } from "../utilities/tokenUtilities.js";

export const checkId = (req, res) => {
	req.params.id === null
		? res.status(400).send("No id")
		: getUserById(req, res);
};

export const getUserByUsername = (req, res) => {
	!req.params.username
		? res.status(400).send("Give a username")
		: getUser(req, res);
};

export const register = (req, res) => {
	if(!checkPassword(req.body.password)) res.status(400).send("Password must be at least 8 characters");
	else {
		!req.body.username || !req.body.email || !req.body.password
			? res.status(400).send("Enter all fields")
			: checkUsername(req, res);
	}
};

export const login = (req, res) => {
	!req.body.email || !req.body.password
		? res.send("Enter email and password")
		: loginUser(req, res);
};

export const deleteUser = async (req, res) => {
	const id = idFromToken(req);
	const user = await findUserById(id);
	(parseInt(req.params.id) === id || user.admin === true)
		? deleteUserFromDb(req, res)
		: res.status(403).send("forbidden");
	
};